import {createCustomElement} from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import { createGraphQLEffect } from '@seismic/effect-graphql';
import get from 'lodash/get';

const listQuery = `query ($queryConditions: String!){
	GlideRecord_Query {
		interaction(queryConditions: $queryConditions){
        _results {
          sys_id {
            displayValue
          }
          number {
            value
          }
          consumer {
            value
		  }
        }
      }
    }
  }`;
  //sn_customerservice_case

  const consumerQuery = `query ($queryConditions: String!){
	GlideRecord_Query {
		csm_consumer(queryConditions: $queryConditions){
        _results {
          sys_id {
            displayValue
          }
          name {
            displayValue
          }
          u_date_of_birth {
            displayValue
		  }
		  u_preferred_airline_seat {
            displayValue
		  }
		  u_special_assistance {
            displayValue
          }
        }
      }
    }
  }`;

  
  
  
   const fetchTableItemEffect = createGraphQLEffect(listQuery, {
	variableList: ['queryConditions'],
    successActionType: 'TABLE_ITEM_FETCH_SUCCEEDED',
    errorActionType: 'TABLE_ITEM_FETCH_FAILED'
  });

  const getConsumers = createGraphQLEffect(consumerQuery, {
    variableList: ['queryConditions'],
    successActionType: 'CONSUMER_FETCH_SUCCEEDED',
    errorActionType: 'CONSUMER_FETCH_FAILED'
  });


  let handleFetchTableItemSucceeded = ({action, updateState, dispatch}) =>{
	  console.log("state");
	  console.log(action)
	   const queryConditions = "sys_id=" + action.payload.data.GlideRecord_Query.interaction._results[0].consumer.value;
	   console.log("query conditions" + queryConditions);
		dispatch('FETCH_CONSUMER', {queryConditions});

	updateState({
        consumer: action.payload.data.GlideRecord_Query.interaction._results[0].consumer.value,
		results: action.payload.data.GlideRecord_Query.interaction._results,
		
	});
	//dispatch('FETCH_CONSUMER', {queryConditions});

};
const handleFetchTableItemFailed = ({action}) => alert('Table Item fetch failed!');



/***********consumr */
let handleConsumerSucceeded = ({action, updateState}) =>{
 updateState({
	 name: action.payload.data.GlideRecord_Query.csm_consumer._results[0].name.displayValue,
	 birth: action.payload.data.GlideRecord_Query.csm_consumer._results[0].u_date_of_birth.displayValue,
	 airline_seat: action.payload.data.GlideRecord_Query.csm_consumer._results[0].u_preferred_airline_seat.displayValue,
	 special_assistance: action.payload.data.GlideRecord_Query.csm_consumer._results[0].u_special_assistance.displayValue,	 
	 results: action.payload.data.GlideRecord_Query.csm_consumer._results
 });
};
const handleConsumerFailed = ({action}) => alert('Consumer fetch failed!');




const view = (state, {updateState}) => {
	const {consumer} = state;
	const{birth} = state;
	const{airline_seat}= state;
	const{special_assistance} = state;

	return (
<		div className="ribbon-container">
			<div className="title">Travel Preferences</div>
			<div className="body-container">
                  <div><div>Date of Birth</div>{birth}</div>
                  <div><div>Preferred Airline Seat</div>{airline_seat}</div>
                  <div><div>Special Assistance</div>{special_assistance}</div> 
                </div>
		</div>
	);
};

createCustomElement('x-1234-factory-component-interaction-cc', {
	renderer: {type: snabbdom},
	view,
	styles,
	initialState: {
		results: [],
		date: ''

	},
	properties: {
		sysid: {default:''},
		table: {default:''},
	  },
	actionHandlers: {
		'TABLE_ITEM_FETCH_REQUESTED': fetchTableItemEffect,
		'TABLE_ITEM_FETCH_SUCCEEDED': handleFetchTableItemSucceeded,
		'TABLE_ITEM_FETCH_FAILED': handleFetchTableItemFailed,
		'LOAD_INITIAL': loadInitialData,

		'FETCH_CONSUMER': getConsumers,
		'CONSUMER_FETCH_SUCCEEDED': handleConsumerSucceeded,
		'CONSUMER_FETCH_FAILED' :handleConsumerFailed
		
	},
	onBootstrap(host,dispatch){
		dispatch('LOAD_INITIAL',{host});
	},
	onConnect(host, dispatch) {
		// request case contact	
   } 
});

async function loadInitialData(coeffects) {
	console.log("here loading")
	const {state, action, dispatch} = coeffects;
	const queryConditions = "sys_id=" + state.properties.sysid;
	
	dispatch('TABLE_ITEM_FETCH_REQUESTED', { queryConditions });
}